import React from "react";
import { Image, StyleSheet, Text } from "react-native";
import { Image_url } from "../interfaces/Image_url";

export function OptionFooterBar({image_url, name_url}:Image_url) {
    return (
        <>
            <Image source={image_url} style={styles.image} />
            <Text style={styles.tabs}>{name_url}</Text>
        </>
    );
}

const styles = StyleSheet.create({
    image: {
        height: 25, 
        width: 25, 
        resizeMode: 'contain',
        marginTop: 7
    },
    tabs: {
        color: 'white',
        fontSize: 11
    }
})