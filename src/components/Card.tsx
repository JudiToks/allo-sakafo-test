import React from "react";
import { Dimensions, Image, StyleSheet, Text, View } from "react-native";

export function Card() {
    return (
        <View style={styles.container}>
            <Image style={styles.image} source={require('../assets/img/pancake.jpg')}/>
            <Text style={{fontWeight: 700, fontSize: 18}}>PanCake</Text>
            <Text>Ar 10 000</Text>
        </View>
    );
}

const screenDimensions = Dimensions.get('screen');
const styles = StyleSheet.create({
    container: {
        // backgroundColor: 'blue',
        width: screenDimensions.width/2,
        padding: 8
    },
    image: {
        height: 120,
        width: '100%', 
        resizeMode: 'contain',
        borderRadius: 10
    }
})