import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";

export function Entete() {
    return (
        <View style={styles.container}>
            <View style={styles.header_container}>
                <Image style={[styles.image, {marginLeft: 10}]} source={require('../assets/icon/menu.png')}/>
                <Text style={styles.text_header}>Header</Text>
                <Image style={[styles.image, {marginRight: 10}]} source={require('../assets/icon/user.png')}/>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'ghostwhite',
        top: 0,
        minHeight: 50
    },
    header_container: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    image: {
        height: 25, 
        width: 25, 
        resizeMode: 'contain',
        marginTop: 10
    },
    text_header: {
        fontSize: 30,
        marginTop: 3
    }
});