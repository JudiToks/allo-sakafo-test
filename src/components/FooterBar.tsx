import { useNavigation } from "@react-navigation/native";
import React from "react";
import { Button, Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { OptionFooterBar } from "./OptionFooterBar";

// image import
import image_home from "../assets/icon/home.png";
import image_menu from "../assets/icon/menu_resto.png";
import image_details from "../assets/icon/details.png";

export function FooterBar() {
    const navigation = useNavigation();
    return (
        <View style={styles.container}>
            <View>
                <TouchableOpacity style={styles.menu_container} onPress={() => navigation.navigate('Home')}>
                    <OptionFooterBar image_url={image_home} name_url="Home"/>
                </TouchableOpacity>
            </View>
            <View>
                <TouchableOpacity style={styles.menu_container} onPress={() => navigation.navigate('Menu')}>
                    <OptionFooterBar image_url={image_menu} name_url="Menu"/>
                </TouchableOpacity>
            </View>
            <View>
                <TouchableOpacity style={styles.menu_container} onPress={() => navigation.navigate('Details')}>
                    <OptionFooterBar image_url={image_details} name_url="Details"/>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        bottom: 0,
        backgroundColor: 'black',
        minHeight: 80,
        maxHeight: 80,
        width: '100%',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    menu_container: {
        flexDirection: 'column',
        alignItems: 'center'
    },
    image: {
        height: 25, 
        width: 25, 
        resizeMode: 'contain',
        marginTop: 7
    },
    tabs: {
        color: 'white',
        fontSize: 11
    }
})