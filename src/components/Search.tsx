import React from "react";
import { Alert, Button, Dimensions, Image, StyleSheet, TextInput, View } from "react-native";
import { SearchBar } from "react-native-screens";

export function Search() {
    return (
        <View style={styles.container}>
            <TextInput style={styles.input} placeholder="Search food" />
            <Button title="Press me" onPress={() => Alert.alert('Simple Button pressed')} />
        </View>
    );
}

const screenDimensions = Dimensions.get('screen');
const styles = StyleSheet.create({
    container: {
        // width: screenDimensions.width,
        backgroundColor: 'blue',
        flexDirection: 'row'
    },
    input: {
        backgroundColor: '#D8D8D8',
        height: 40,
        width: '80%',
        margin: 12,
        borderWidth: 1,
        borderColor: '#D8D8D8',
        borderRadius: 100,
        padding: 10,
    },
})