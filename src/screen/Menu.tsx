import React from "react";
import { ScrollView, StyleSheet, Text, View, useColorScheme } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { Colors, DebugInstructions, LearnMoreLinks, ReloadInstructions } from "react-native/Libraries/NewAppScreen";
import { FooterBar } from "../components/FooterBar";
import { Card } from "../components/Card";
import { useNavigation } from "@react-navigation/native";

export function Menu() {
    const isDarkMode = useColorScheme() === 'dark';
    const backgroundStyle = {
      backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
    };
    const navigation = useNavigation();

    React.useLayoutEffect(() => {
        navigation.setOptions({
            headerLargeTitle: true,
            headerSearchBarOptions: {
                placeHolder: "Search"
            }
        })
    }, [navigation])

    return (
        <SafeAreaView style={[backgroundStyle, styles.container]}>
            <ScrollView contentInsetAdjustmentBehavior="automatic" contentContainerStyle={[backgroundStyle, styles.content]}>
                <Card />
                <Card />
                <Card />
                <Card />
                <Card />
                <Card />
                <Card />
                <Card />
                <Card />
                <Card />
                <Card />
            </ScrollView>
            <FooterBar />
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    highlight: {
        fontWeight: '700',
    },
    content: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    }
});