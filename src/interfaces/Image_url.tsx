import { ImageSourcePropType } from "react-native";

export interface Image_url {
    image_url: ImageSourcePropType
    name_url: string
}